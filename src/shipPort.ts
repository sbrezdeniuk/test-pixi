import { Container, Graphics } from "pixi.js";

enum Piers {
  pier1 = "pier1",
  pier2 = "pier2",
  pier3 = "pier3",
  pier4 = "pier4",
}

export class ShipPort {
  readonly portContainer: Container;
  readonly port: Graphics;
  pier1: Graphics;
  pier2: Graphics;
  pier3: Graphics;
  pier4: Graphics;
  constructor() {
    this.portContainer = new Container();
    this.port = new Graphics();
    this.pier1 = new Graphics();
    this.pier2 = new Graphics();
    this.pier3 = new Graphics();
    this.pier4 = new Graphics();

    this.port.lineStyle(10, '#D6B500', 1);
    this.port.moveTo(200, 0);
    this.port.lineTo(200, 200);

    this.port.lineStyle(10, '#D6B500', 1);
    this.port.moveTo(200, 600);
    this.port.lineTo(200, 400);

    this.pier1.lineStyle(10, '#D6B500', 1);
    this.pier1.beginFill('#D6B500', 1)
    this.pier1.drawRect(0, 0, 45, 130);
    this.pier1.endFill();

    this.pier2.lineStyle(10, '#D6B500', 1);
    this.pier2.drawRect(0, 150, 45, 130);
    this.pier2.endFill();
    
    this.pier3.lineStyle(10, '#D6B500', 1);
    this.pier3.beginFill('#D6B500', 1);
    this.pier3.drawRect(0, 300, 45, 130);
    this.pier3.endFill();

    this.pier4.lineStyle(10, '#D6B500', 1);
    this.pier4.drawRect(0, 450, 45, 130);
    this.pier4.endFill();

    this.portContainer.addChild(this.port);
    this.portContainer.addChild(this.pier1);
    this.portContainer.addChild(this.pier2);
    this.portContainer.addChild(this.pier3);
    this.portContainer.addChild(this.pier4);
  }

  setFullOrEmptyPire(portNumber: string , isFull: boolean = false): void {
    if(isFull) {
      this[portNumber as Piers].beginFill('#D6B500', 1);
    } else {
      this[portNumber as Piers].beginFill(0, 0);
    }
  }

  getView() {
    return this.portContainer
  }
}
