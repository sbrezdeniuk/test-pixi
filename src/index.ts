import * as PIXI from 'pixi.js';
import { ShipPort } from './shipPort';
import { Ship } from './Ship';

const app = new PIXI.Application({
  width: 800,
  height: 600,
  backgroundColor: '#4D35FF',
  view: document.getElementById('game-canvas') as HTMLCanvasElement,
})
/*
yellow - #D6B500;
red - #FF0000
green - #59FF59
*/

let shipPort = new ShipPort();

let shipGreen = new Ship("#59FF59");

let shipRed = new Ship("#FF0000");


shipRed.setFill(true);
shipRed.setPosition(350);


app.stage.addChild(shipRed.getView())
app.stage.addChild(shipGreen.getView())
app.stage.addChild(shipPort.getView())

