import { Graphics, Sprite, RenderTexture, autoDetectRenderer } from "pixi.js";

export class Ship {
  ship: Graphics;
  color: string;
  constructor(color: string, isFull: boolean = false) {
    this.ship = new Graphics();
    this.color = color;
    this.ship.lineStyle(10, color, 1);
    if(isFull) {
      this.ship.beginFill(color, 1)
    }
    this.ship.drawRect(650, 200, 130, 45);
    this.ship.endFill();
  }

  setFill(isFull: boolean = false) {
    if(isFull) {
      this.ship.beginFill(this.color, 1);
    } else {
      this.ship.lineStyle(0, 0);
    }
  }
  setPosition(x: number, y: number = 200) {
    this.ship.drawRect(x, y, 130, 45);
  }
  getView() {
    return this.ship
  }
}